import React from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  FlatList,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import moment from 'moment';
//components
import AccountIcon from './Icon';
//types
import { Transaction } from '../../core/models/Transaction.model';
//styles
import GlobalStyles from '../../styles/GlobalStyles';
import Colors from '../../styles/Colors';
//utils
import { getAmountColor, amountToShow } from '../../core/utils/amount';

interface IProps {
  item: Transaction;
  index: number;
  onPress: () => void;
}

const getTransactionToShow = (item: Transaction) => {
  let transaction = {
    iconType: '',
  };

  switch (item.resourcetype) {
    case 'Expenditure':
      transaction.iconType = 'category';

      break;
    case 'Income':
      break;
    case 'Transfer':
      break;
  }
};

const TransactionComponent = (props: IProps) => (
  <Animatable.View
    style={styles.itemContainer}
    animation="fadeIn"
    duration={500}
    delay={100 * props.index + 200}
    useNativeDriver>
    <TouchableOpacity
      style={styles.item}
      onPress={props.onPress}
      activeOpacity={0.7}>
      <View style={styles.insideItemContainer}>
        <AccountIcon
          type={
            props.item.resourcetype === 'Expenditure' ? 'category' : 'account'
          }
          size="small"
          color={props.item.other.color}
          icon={props.item.other.icon}
        />
        <View style={styles.textItemContainer}>
          <Text style={[GlobalStyles.textContentSmallSecondary]}>
            {props.item.source.name}
          </Text>
          <Text style={[GlobalStyles.accountHeader, styles.itemTextSeparate]}>
            {props.item.other.name}
          </Text>
          <Text style={[GlobalStyles.textContentSmallSecondary]}>
            {moment(props.item.transaction_date).format('DD.MM.YYYY HH:mm')}
          </Text>
        </View>
      </View>

      <View style={styles.row}>
        <Text style={[GlobalStyles.headerSmall, styles.amountText]}>
          {amountToShow(props.item.value)}
        </Text>
        <View
          style={[
            styles.typeIndicator,
            { backgroundColor: getAmountColor(props.item.value) },
          ]}
        />
      </View>
    </TouchableOpacity>
  </Animatable.View>
);

export default TransactionComponent;

const styles = StyleSheet.create({
  itemContainer: {
    borderRadius: 10,
    marginVertical: 7,
    overflow: 'hidden',
    marginHorizontal: 15,
  },
  item: {
    height: 70,
    backgroundColor: Colors.itemGray,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8,
  },
  itemTextSeparate: {
    marginVertical: 3,
  },
  insideItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textItemContainer: {
    marginLeft: 10,
  },
  typeIndicator: {
    height: 55,
    width: 6,
    borderRadius: 6,
    backgroundColor: Colors.primary,
  },
  amountText: {
    marginRight: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
