import { Category } from './Category.model';
import { Account } from './Account.model';
import { Currency } from './Currency.model';

export interface Transaction {
  id: number;
  description: string;
  value: number;
  transaction_date: string;
  source: Account | Category;
  other: Account | Category;
  resourcetype: 'Income' | 'Expenditure' | 'Transfer';
}
