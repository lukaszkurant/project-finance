export interface UserModel {
  token?: string;
  refreshToken?: string;
}
