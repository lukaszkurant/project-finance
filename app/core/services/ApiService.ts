import axios, {
  AxiosRequestConfig as AxiosRequestConfigFromAxios,
} from 'axios';
//services
import UserService from './UserService';
//config
import Config from '../config';

export interface AxiosRequestConfig extends AxiosRequestConfigFromAxios {}

const client_secret =
  'KtW0v4Z3cRacasRhF7X31zR8uX5senxt3DK449SZ8uHpugWHVCu1KcpoEGSUQfQdSbRaeM8rL846OcqvVYR1zqzRooWO5fuP4D0Yisjmlxa6tolNWx5VloStNdpm9AYU';

const client_id = 'sy5KF5XWfKiiAg3azeYUwmbcXifgLkUbG9uGXczC';

const Points = {
  USERS: 'api/users/',
  OAUTH: 'o/',
  AUTHORIZATION: 'token/',
  FORGOTPASSWORD: '',
  RESETPASSWORD: '',
  ACCOUNTS: 'api/accounts/',
  CATEGORIES: 'api/categories/',
  TRANSACTIONS: 'api/transactions/',
  STATS_EXPEDINTURES: 'stats/expenditures/',
  STATS_INCOMES: 'stats/incomes/',
  CHANGE_PASSWORD: 'api/users/change_password/',
  RECEIPT: 'api/receipts/',
};

const refreshToken = async () => {
  const data = UserService.getUserData();
  const { token } = data;
  return new Promise((resolve, reject) => {
    let refData: { [key in string]: any } = {
      client_id: client_id,
      client_secret: client_secret,
      refresh_token: token,
      grant_type: 'refresh_token',
    };
    let body: AxiosRequestConfig = {
      url: Points.USERS,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: Object.keys(refData)
        .map(function(key) {
          return (
            encodeURIComponent(key) + '=' + encodeURIComponent(refData[key])
          );
        })
        .join('&'),
    };
    request(body)
      .then(response => resolve(response))
      .catch(err => reject(err));
  });
};

let isRefreshing = false;
let subscribers: any = [];

const request = axios.create({
  baseURL: Config.API_BASE_URL,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
});

request.interceptors.request.use(
  config => {
    const userData = UserService.getUserData();
    const { token } = userData;
    console.log('token', token);
    if (token) {
      config.headers.Authorization = 'Bearer ' + token;
    }
    if (config.url) {
      const url = config.url.split('/');
      if (url[url.length - 1] === 'token') {
        delete config.headers.Authorization;
      }
    }
    console.log('conf', config);
    return config;
  },
  error => Promise.reject(error)
);

request.interceptors.response.use(undefined, response => {
  const {
    config,
    response: { status, data },
  } = response;
  console.log('response', config, status, data);
  const originalRequest = config;
  if (status === 401) {
    if (!isRefreshing) {
      isRefreshing = true;
      refreshToken()
        .then((responseRefresh: any) => {
          const { refreshData } = responseRefresh.data;
          isRefreshing = false;
          onRrefreshed(data.access_token);
          UserService.setUserData({
            token: refreshData.access_token,
            refreshToken: refreshData.refresh_token,
          });
          subscribers = [];
        })
        .catch(() => {
          UserService.logout();
        });
    }
    const requestSubscribers = new Promise(resolve => {
      subscribeTokenRefresh((token: string) => {
        originalRequest.headers.Authorization = `Bearer ${token}`;
        resolve(axios(originalRequest));
      });
    });
    return requestSubscribers;
  } else {
    return Promise.reject(response);
  }
});

function subscribeTokenRefresh(cb: any) {
  subscribers.push(cb);
}

function onRrefreshed(token: string) {
  subscribers.map((cb: any) => cb(token));
}

export default {
  request,
  Points,
  client_secret,
  client_id,
};
